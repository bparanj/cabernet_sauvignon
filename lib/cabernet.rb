require "cabernet/version"
require 'paypal-sdk-permissions'

module Cabernet
  
  class PaypalPermissionGateway
    # 
    # 
    # access_token_callback_url : Callback URL is the return URL to your server 
    #
    # Returns : Request token string (used in the next call)
    #
    def self.request_permissions(access_token_callback_url)
      # Build request object
      permissions_request = api.build_request_permissions
      permissions_request.scope    = ["EXPRESS_CHECKOUT"]
      permissions_request.callback = access_token_callback_url
      
      # Make API call & get response
      permissions_response = api.request_permissions(permissions_request)
      
      # Access Response
      permissions_response.token
    end  
    #
    # Update the merchant record that was created during registration with the
    # permission token and secret for the current user.
    #
    # request_token     : token that was obtained in previous call
    # verification_code : code that was obtained in the callback to your server
    #
    # Returns           : Access token response object
    #
    def self.get_access_token(request_token, verification_code)
      # Build request object
      token_request           = api.build_get_access_token
      token_request.token     = request_token
      token_request.verifier  = verification_code
      
      # Make API call & get token_response
      api.get_access_token(token_request)
    end

    private 
    
    def self.api
      @api ||= PayPal::SDK::Permissions::API.new
    end

  end
end