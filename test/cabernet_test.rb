require_relative 'test_helper'

class CabernetTest < Minitest::Test

  def test_request_permissions_returns_token
    VCR.use_cassette("permissions") do
      token = Cabernet::PaypalPermissionGateway.request_permissions('http://localhost/callback')
    
      assert_equal 'AAAAAAAeqDRF776hrQxo', token
    end    
  end
  
  def test_get_access_token_returns_token_response
    VCR.use_cassette("permissions", :record => :new_episodes) do
      response = Cabernet::PaypalPermissionGateway.get_access_token('AAAAAAAeqDgyQ0mQLHYs', 'BTRfWJ9t6CjzQGaSw2aBMg')
      
      assert_equal 'vYHKeWiEst9V28d0xkH29I5oMxTk2EHtfvolbF8yD-zZQbvGfztftQ', response.token
      assert_equal 'lGEdYyN.SmRlB87GAFGPcF99qI0', response.token_secret   
    end
  end
end