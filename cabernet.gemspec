# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'cabernet/version'

Gem::Specification.new do |spec|
  spec.name          = "cabernet"
  spec.version       = Cabernet::VERSION
  spec.authors       = ["Bala Paranj"]
  spec.email         = ["bparanj@gmail.com"]

  spec.summary       = %q{A simple wrapper for paypal-sdk-permissions gem}
  spec.description   = %q{This library enables developers to execute Paypal APIs on behalf of merchants.}
  spec.homepage      = "http://www.rubyplus.com."
  spec.license       = "MIT"

  spec.files         = `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.required_ruby_version = '>= 2.2.2'
  
  spec.add_dependency 'paypal-sdk-permissions', "~> 1.96.4"
  
  spec.add_development_dependency "bundler", "~> 1.12.5"
  spec.add_development_dependency "rake", "~> 10.5.0"
  spec.add_development_dependency "minitest", "~> 5.8"
  spec.add_development_dependency "vcr", "~> 3.0.1"
  spec.add_development_dependency "webmock", "~> 1.24"
  spec.add_development_dependency "simplecov", "~> 0.11.2"
  spec.add_development_dependency "rubycritic"
end
