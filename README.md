# Cabernet

To experiment with that code, run `bin/console` for an interactive prompt.

## Configuration

For Rails app, create config/paypal.yml:

```ruby
development: &default
  username: jb-us-seller_api1.paypal.com
  password: WX4WTU3S8MY44S7F
  signature: AFcWxV21C7fd0v3bYYYRCpSSRl31A7yDhhsPUU2XhtMoZXsWHFxu-RWy
  app_id: APP-80W284485P519543T
  http_timeout: 30
  mode: sandbox
test:
  <<: *default
production:
  <<: *default
  mode: live
```

Instead of hardcoding the values, you can use environment variables: 

```ruby
development: &default
  username: ENV['permissions_username']
  password: ENV['permissions_password]
  signature: ENV['permissions_signature']
  app_id:    ENV['app_id']
  http_timeout: 30
  mode: sandbox
```

The Paypal SDK library will automatically load the values in config/paypal.yml, when this call is made:

```ruby
PayPal::SDK::Permissions::API.new
```

It will not automatically load the file if the config file is named something else.

## Version

Ruby : 2.2.3, 2.3.1

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'cabernet'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install cabernet

## Usage

Refer the sample Rails app : https://bitbucket.org/bparanj/caber (Ruby 2.2.3 and Rails 4.2.4)

## Running Tests

```ruby
$rake test
```

Open coverage/index.html to view the test coverage report.

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake test` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome on bitbucket at https://bitbucket.org/bparanj/cabernet_sauvignon.


## License

The gem is available as open source under the terms of the [MIT License](http://opensource.org/licenses/MIT).
